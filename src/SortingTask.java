import java.io.*;
import java.util.*;

public class SortingTask
{
    private static final int[] COUNTS = new int[]{
            50,100,250,500,1000,2000,5000,7500,10000,
            15000,20000,30000,45000,60000,75000,90000,100000,
            125000,150000,175000,200000,225000,250000
    };
    private static final int NUMTESTS = COUNTS.length;
    private static final int MAX = COUNTS[NUMTESTS-1];

    private static int COUNT;
    private static int ROUND;
    private static boolean VERBOSE = true;
    private static boolean WARMUP  = true;
    private static List<Integer> List;
    private static List<Pair>    Pairs;
    private static Integer[]     Array;
    private static List<Integer> RandList = new ArrayList<Integer>(MAX);

    private interface SortingMethod { void sort(); }

    // Results["sortingMethod"][ROUND] == averageMillis;
    private static Hashtable<String, double[]> Results = new Hashtable<String, double[]>();

    public static void main (String[] args)
    {
        Random generator = new Random();
        int maxKey = Math.min(1000, (MAX + 32) / 16);
        for (int i = 0; i < MAX; ++i)
            RandList.add(generator.nextInt(maxKey));

        final boolean DEBUG = true;
        if (DEBUG)
        {
            System.out.println("Running performance test");
            WARMUP = false;
            runTests(true);
        }
        else
        {
            System.out.println("Warming up tests");
            runTests(false);
            System.out.println("Running performance test");
            runTests(true);
            System.out.println("Running silent tests");
            for (int i = 0; i < 5; ++i) runTests(false);
            System.out.println();
            writeResultsToCSV("sorting.csv");
            System.out.println("Wrote sorting.csv");
        }
    }

    private static void measure(String name, SortingMethod method) { measure(name, method, false); }
    private static void measure(String name, SortingMethod method, boolean useArray)
    {
        List = new ArrayList<Integer>(RandList).subList(0, COUNT);
        if (useArray) Array = List.toArray(new Integer[COUNT]);

        long start = System.nanoTime();
        method.sort();
        double millis = (System.nanoTime() - start) / 1000000.0; // milliseconds

        // console output and writing results:
        if (!WARMUP) {
            if (VERBOSE) System.out.printf("%24s: %.3fms\n", name, millis);
            updateResults(name, millis);
        }
        if (!VERBOSE) System.out.print('.');

        if (!isSorted(useArray ? Arrays.asList(Array) : List, COUNT))
            throw new RuntimeException(name + ": Wrong order!!!");
    }

    private static void stability(String name, SortingMethod method)
    {
        Pairs = new ArrayList<Pair>(COUNT);
        for (int i = 0; i < COUNT; ++i)
            Pairs.add(new Pair(RandList.get(i), i));
        method.sort();
        if (!isStable(Pairs, 0, COUNT))
            throw new RuntimeException(name + ": not stable sorted!");
    }

    private static void runTests(boolean isVerbose)
    {
        SortingMethod quickSort     = new SortingMethod() { public void sort() { qsort(List, 0, COUNT);                }};
        SortingMethod javaArraySort = new SortingMethod() { public void sort() { Arrays.sort(Array, 0, COUNT);         }};
        SortingMethod javaListSort  = new SortingMethod() { public void sort() { Collections.sort(List);               }};
        SortingMethod insertionSort = new SortingMethod() { public void sort() { insertionSort(List, 0, COUNT);        }};
        SortingMethod insertionOrig = new SortingMethod() { public void sort() { insertionOrig(List, 0, COUNT);        }};
        SortingMethod binarySort    = new SortingMethod() { public void sort() { binaryInsertionSort(List, 0, COUNT);  }};
        SortingMethod binaryStab    = new SortingMethod() { public void sort() { binaryInsertionSort(Pairs, 0, COUNT); }};

        VERBOSE = isVerbose;
        for (ROUND = 0; ROUND < NUMTESTS; ++ROUND)
        {
            COUNT = COUNTS[ROUND];

            if (VERBOSE) System.out.printf("\n=========== %6d ===========\n", COUNT);
            measure("Quicksort",             quickSort);
            measure("java.util.Arrays",      javaArraySort, true);
            measure("java.util.Collections", javaListSort);
            if (COUNT <= 45000)
            {
                measure("BinaryInsertionSort",   binarySort);
                stability("BinaryInsertionSort", binaryStab);
                measure("InsertionSort",         insertionSort);
                measure("InsertionSortOrig",     insertionOrig);
            }
            else if (VERBOSE)
            {
                System.out.printf("%24s: too slow\n", "BinaryInsertionSort");
                System.out.printf("%24s: too slow\n", "InsertionSort");
                System.out.printf("%24s: too slow\n", "InsertionSortOrig");
            }
        }
        if (VERBOSE) System.out.printf("\n==============================\n");
        else System.out.println();
        WARMUP = false; // stop warmup period after first test
    }

    /**
     * Sort a part of the list using binary insertion sort method in a stable manner.
     */
    public static <T extends Comparable<T>>
    void binaryInsertionSort(List<T> a, int start, int end)
    {
        for (int i = start; i < end; ++i)
        {
            T v = a.get(i);
            int imin = start, imax = i;
            while (imin < imax) // find insertion point: imin
            {
                int imid = (imin + imax) >> 1;
                if (a.get(imid).compareTo(v) < 0)
                    imin = imid + 1;
                else
                    imax = imid;
            }

            // stable sort: new insert always goes last
            while (imax < i && a.get(imax).compareTo(v) == 0)
                ++imax; // skip equal elements

            // start shuffling element from imax to i
            if (imax < i)
            {
                v = a.get(imax);
                a.set(imax, a.get(i));
                do
                {
                    T next = a.get(++imax);
                    a.set(imax, v);
                    v = next;
                } while (imax < i);
            }
        }
    }

    /** Sort a part of the list of comparable elements using insertion sort. */
    public static<T extends Comparable<T>>
    void insertionSort(List<T> a, int start, int end)
    {
        for (int i = start; i < end; ++i)
        {
            int j = i;
            T v = a.get(i);
            for (T k; j > 0 && v.compareTo(k = a.get(j - 1)) < 0; --j)
                a.set(j, k); // swap: [j-1] --> [j]
            a.set(j, v);
        }
    }

    public static<T extends Comparable<T>>
    void insertionOrig(List<T> a, int l, int r)
    {
        if (a.size() < 2) return;
        if ((r-l)<2) return;
        for (int i = l+1; i < r; i++)
        {
            T b = a.remove (i); // O(n)
            int j;
            for (j=l; j < i; j++) { // O(n)
                if (b.compareTo (a.get (j)) < 0) break;
            }
            a.add (j, b); // O(n)
        }
    }

    /** Sort a part of the list using quicksort method. */
    public static void qsort(List<Integer> array, int l, int r)
    {
        if (array.size() < 2) return;
        if ((r-l)<2) return;
        int i = l; int j = r - 1;
        Integer x = array.get((i+j) / 2);
        do
        {
            while (array.get(i).compareTo(x) < 0) i++;
            while (x.compareTo(array.get(j)) < 0) j--;
            if (i <= j)
            {
                Integer tmp = array.get (i);
                array.set (i, array.get (j));
                array.set (j, tmp);
                i++; j--;
            }
        } while (i < j);
        if (l < j)   qsort(array, l, j+1); // recursion for left part
        if (i < r-1) qsort(array, i, r); // recursion for right part
    }

    /** @return TRUE if list is sorted */
    static<T extends Comparable<T>> boolean isSorted(List<T> a, int count)
    {
        for (int i = 1; i < count; ++i)
            if (a.get(i-1).compareTo(a.get(i)) > 0)
            {
                System.out.print("[");
                for (int j = Math.max(0, i - 3), e = j + 10; j < e; ++j)
                    System.out.print(a.get(j) + (j!=e-1?",":""));
                System.out.println("]");
                return false;
            }
        return true;
    }

    /** Is the list sorted in a stable manner. */
    static boolean isStable(List<Pair> a, int start, int end)
    {
        for (int i = start + 1; i < end; ++i)
        {
            Pair x = a.get(i-1), y = a.get(i);
            if (x.compareTo(y) == 0 && x.second > y.second)
            {
                System.out.printf("Method is not stable: [%d]=%s <--> [%d]=%s\n", i-1, x, i, y);
                return false;
            }
        }
        return true;
    }

    public static class Pair implements Comparable<Pair>
    {
        public final int first, second;
        public int getSecundo() { return second; }
        Pair(int a, int b) {
            first  = a;
            second = b;
        }
        public String toString() {
            return String.format("(%d,%d)", first, second);
        }
        public int compareTo(Pair o) {
            return first - o.first;
        }
    }

    private static void updateResults(String name, double elapsedMillis)
    {
        double[] counts = Results.get(name);
        if (counts == null)
            Results.put(name, counts = new double[NUMTESTS]);
        counts[ROUND] = (counts[ROUND] != 0.0) ? (counts[ROUND]+elapsedMillis)/2 : elapsedMillis;
    }

    private static void writeLine(StringBuilder sb, String name, List<Number> values)
    {
        sb.append(name).append(',');
        for (int i = 0; i < values.size(); ++i)
        {
            double value = values.get(i).doubleValue();
            if (value != 0.0) {
                if ((value - (long)value) < 0.000001) sb.append((int)value);
                else sb.append(String.format(Locale.US, "%.3f", value));
            }
            if (i != values.size() - 1) sb.append(',');
        }
        sb.append("\r\n");
    }
    private static ArrayList<Number> toList(int[] values) {
        ArrayList<Number> list = new ArrayList<Number>(values.length);
        for (int value : values) list.add(value);
        return list;
    }
    private static ArrayList<Number> toList(double[] values) {
        ArrayList<Number> list = new ArrayList<Number>(values.length);
        for (double value : values) list.add(value);
        return list;
    }

    private static void writeResultsToCSV(String fileName)
    {
        StringBuilder sb = new StringBuilder();

        // random,count0,count1,count2,..,countN
        writeLine(sb, "random", toList(COUNTS));;

        // sortName,round0,round1,round2,..,roundN
        for (Map.Entry<String, double[]> e : Results.entrySet())
            writeLine(sb, e.getKey(), toList(e.getValue()));

        try
        {
            FileWriter writer = new FileWriter(fileName);
            writer.write(sb.toString());
            writer.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}